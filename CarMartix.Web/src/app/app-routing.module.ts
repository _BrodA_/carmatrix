import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MyVehiclesComponent } from './Pages/my-vehicles/my-vehicles.component';

const routes: Routes = [
  {
    path: 'myvehicles',
    component: MyVehiclesComponent
  },
  { path: '**', redirectTo: '/' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
